tools needed:
   sudo apt-get install iotop sysstat collectl

run afterwards:
   htop
   sudo iotop
   watch iostat -x
   sudo collectl -sD
   sudo bonnie++ -m ${HOSTNAME} -u root -x 3 -d /tmp/ -s 1024 -r 512 | bon_csv2html > ${HOSTNAME}-result.htm

SAR Commands	Equivalence 			Other Commands		Equivalence
sar -b	        collectl -sd 			iostat -x		collectl -sD
sar -B	        collectl -sm --verbose 		/proc/fs/lustre		collectl -sl --verbose
sar -clquw	collectl -sx --verbose 		mpstat 			collectl -sc --verbose
sar -d	        collectl -sD 			netstat -i 		collectl -sN
sar -cI XALL	collectl -sJ 			nfsstat 		collectl -sfF
sar -n ALL	collectl -sfsN 			perfquery 		collectl -sx
sar -P ALL	collectl -sC 			cat /proc/net/netstat 	collectl -sY
sar -r	        collectl -sm --verbose 		ps 			collectl -sZ
sar -R	        collectl -sm --memopts R 	slabtop 		collectl -sY --slabopts S
sar -v	        collectl -si 			top 			collectl --top -scm
sar -W	        collectl --vmstat 		vmstat 			collectl --vmstat


--------------
sudo /usr/sbin/smartctl -H -d cciss,0 /dev/cciss/c0d0
sudo /usr/sbin/smartctl -all -d cciss,0 /dev/cciss/c0d0   

ubuntutst@ubuntuBareMetal:/dev/cciss$ sudo /usr/sbin/smartctl -H -d cciss,0 /dev/cciss/c0d0
smartctl 5.41 2011-06-09 r3365 [x86_64-linux-3.5.0-23-generic] (local build)
Copyright (C) 2002-11 by Bruce Allen, http://smartmontools.sourceforge.net

SMART Health Status: OK
ubuntutst@ubuntuBareMetal:/dev/cciss$ sudo /usr/sbin/smartctl -all -d cciss,0 /dev/cciss/c0d0
smartctl 5.41 2011-06-09 r3365 [x86_64-linux-3.5.0-23-generic] (local build)
Copyright (C) 2002-11 by Bruce Allen, http://smartmontools.sourceforge.net

=======> INVALID ARGUMENT TO -l: l
=======> VALID ARGUMENTS ARE: error, selftest, selective, directory[,g|s], background, scttemp[sts|hist], scterc[,N,M], sasphy[,reset], sataphy[,reset], gplog,N[,RANGE], smartlog,N[,RANGE], xerror[,N][,error], xselftest[,N][,selftest] <=======

Use smartctl -h to get a usage summary

sudo /usr/sbin/smartctl -l error -d cciss,0 /dev/cciss/c0d0

ubuntutst@ubuntuBareMetal:/dev/cciss$ sudo /usr/sbin/smartctl -l error -d cciss,0 /dev/cciss/c0d0
smartctl 5.41 2011-06-09 r3365 [x86_64-linux-3.5.0-23-generic] (local build)
Copyright (C) 2002-11 by Bruce Allen, http://smartmontools.sourceforge.net


Error counter log:
           Errors Corrected by           Total   Correction     Gigabytes    Total
               ECC          rereads/    errors   algorithm      processed    uncorrected
           fast | delayed   rewrites  corrected  invocations   [10^9 bytes]  errors
read:          0        0         0         0          0          0.000           0
write:         0        0         0         0          0          0.000           0

Non-medium error count:      314

------------------

---------------
sudo /usr/sbin/smartctl -H  /dev/sda
sudo /usr/sbin/smartctl -i /dev/sda


