CONFIG_FILE="/opt/yocto/build/master-2013-05-03_LOCAL-2013-04-25-c09866b-armv7a-beaglebone/conf/local.conf"
# BB_NUMBER_THREADS = "8"
perl -pi -e 's/(BB_NUMBER_THREADS )= \"(\d+)/"BB_NUMBER_THREADS = \"" . ($2 + 1)/e' ${CONFIG_FILE}
# PARALLEL_MAKE = "-j 8"
perl -pi -e 's/(PARALLEL_MAKE )= \"-j (\d+)/"PARALLEL_MAKE = \"-j " . ($2 + 1)/e' ${CONFIG_FILE}
#vim ${CONFIG_FILE}
