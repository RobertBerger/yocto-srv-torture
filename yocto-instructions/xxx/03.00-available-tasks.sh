HERE=`pwd`

if test -t 1 -a "$TERM" == "screen"
then
   echo "well done - you use screen"
   echo "you might like byobu as well"
elif test -t 1 -a "$TERM" == "screen-bce"
then
   echo "well done - you use byobu"
else
   echo "you are not running the stuff from screen"
   echo "execute screen or attach to a running screen"
   exit
fi

   echo "you are running the stuff from screen"
   echo "... we'll go ahead ..."

cd /work/${USER}/eldk
BUILD_NAME=$(git branch | sed -ne 's/^\* //p')-$(git log --format="%ad-%h" --date=short HEAD^\!)
echo $BUILD_NAME
# eldk-5.1.1_LOCAL-2012-01-02-8281cbd
#sudo mkdir -p /opt/eldk/build
#sudo chown -R ${USER}:${USER} /opt/eldk
#source poky-init-build-env /work/rber/build/eldk-2011-05-13-213a268
source oe-init-build-env /opt/eldk/build/${BUILD_NAME}-armv7a
#cd /work/rber/build/eldk-2011-05-13-213a268

#machine=powerpc
bitbake -s > ${HERE}/packages.txt
