=========== Yocto with meta-ti (without meta-oe) on the beaglebone ==========

copy over the .gitconfig

sudo mkdir -p /work/${USER}
sudo chown -R ${USER}:${USER} /work/${USER}
cd /work/${USER}

git clone git://git.yoctoproject.org/poky
cd poky
git checkout master
git pull
git checkout master -b master-$(date +"%Y-%m-%d")_LOCAL
git branch

git log --pretty='format:%d  %h   %ci   %s' "$@" | head -1
#  (HEAD, danny-8.0.1, danny-8.0.1_LOCAL)  b057488   2013-04-04 14:13:48 +0100   documentation: Updated the Manual Revision History Dates

git clone git://git.yoctoproject.org/meta-ti meta-ti
# for yocto danny we also need to meta-ti danny branch
cd meta-ti
git co master
git pull
git branch $(date +"%Y-%m-%d")_LOCAL
git co $(date +"%Y-%m-%d")_LOCAL
git branch
cd ..

git log --pretty='format:%d  %h   %ci   %s' "$@" | head -1
#  (HEAD, origin/master, origin/HEAD, master-2013-04-19_LOCAL, master)  3b2d8e5   2013-04-18 22:02:43 +0100   build-appliance: Update to latest dylan revision for release

BUILD_NAME=$(git branch | sed -ne 's/^\* //p')-$(git log --format="%ad-%h" --date=short HEAD^\!)

echo $BUILD_NAME
# master-2013-04-19_LOCAL-2013-04-18-3b2d8e5

sudo mkdir -p /opt/yocto/build
sudo chown -R ${USER}:${USER} /opt/yocto

source oe-init-build-env /opt/yocto/build/${BUILD_NAME}-armv7a-beaglebone

# the previous line should take you straight to 
# /opt/yocto/build/master-2013-04-19_LOCAL-2013-04-18-3b2d8e5-armv7a-beaglebone

cp -vp conf/bblayers.conf{,.ORIG}

vim conf/bblayers.conf

It should look like this after the changes:

BBFILES ?= ""
BBLAYERS ?= " \
  /work/rber/poky/meta \
  /work/rber/poky/meta-yocto \
  /work/rber/poky/meta-yocto-bsp \
  /work/rber/poky/meta-ti \
  "
cp -vp conf/local.conf{,.ORIG}

vim conf/local.conf

add your usual changes and:

MACHINE ?= "beaglebone"
BBMASK = "meta-ti/recipes-misc"

PACKAGE_CLASSES ?= "package_ipk"

INHERIT += "rm_work"
--------------------

try a build:

time bitbake -c fetchall core-image-minimal
time bitbake -k core-image-minimal

time bitbake -c fetchall core-image-sato-sdk
time bitbake -k core-image-sato-sdk

time bitbake -c fetchall meta-toolchain-sdk
time bitbake -k meta-toolchain-sdk

time bitbake -c fetchall world
time bitbake -k world

if you get udev errors:

vim conf/local.conf
PREFERRED_VERSION_udev = "164"

----------------------
-----------------------
---------------------------
--------------------------------


# Add MACHINE ?= "beaglebone" to your conf/local.conf
# Add the meta-oe and meta-ti layers to your conf/bblayers.conf, mine ended up
as follows:

        # LAYER_CONF_VERSION is increased each time build/conf/bblayers.conf
        # changes incompatibly
        LCONF_VERSION = "4"

        BBFILES ?= ""
        BBLAYERS ?= " \
          /media/scratch/poky-git/meta \
          /media/scratch/poky-git/meta-yocto \
          /media/scratch/poky-git/layers/meta-oe \
          /media/scratch/poky-git/meta-ti \
          "

--------------------
Try this:

no meta-oe.git
only meta-ti

but in local.conf:

BBMASK = "meta-ti/recipes-misc"

-------------------


# I had to manually mess with the qemu configuration to get the toolchain/sdk to build
# This patch explains what I had to better than I can
http://git.yoctoproject.org/cgit.cgi/poky-contrib/commit/?h=robert/qemu&id=23138099c77cd3dc6af309c81ef52da03a9ad004
# !!! If you get libcurl errors, it's likely this - clean and restart your build with 
bitbake -c cleansstate qemu-nativesdk

# !!! If you get udev errors as well *urrgh* then you can set a specific version 
usage by adding PREFERRED_VERSION_udev = "164" to your local.conf.

# Build stuff
bitbake -k core-image-minimal meta-toolchain

# Grab what you need from the tmp/deploy/images
* core-image-minimal-beaglebone.tar.bz2
* uImage-beaglebone.bin
* modules-beaglebone.tgz # Only if you need these, I didn't.
# And the SDK from tmp/deploy/sdk
* poky-eglibc-i686-arm-toolchain-1.2+snapshot-2012XXXX.tar.bz2

# Tar up the kernel from the work directory
tar cfz kernel_3.2.14.tar.gz tmp/work/beaglebone-poky-linux-gnueabi/linux-ti33x-psp-3.2-r0/git

# Move the stuff over to your other machine
I'll let you figure that out ;)

# -------------------------------------------------------------------------------
# Setup up the toolchain on local machine
cd / 
sudo tar xfvj ~/Documents/BeagleBone/git-poky/poky-eglibc-i686-arm-toolchain-1.2+snapshot-20120516.tar.bz2
# Source the environment script to make life easy (set up PATH etc)
source  /opt/poky/1.2+snapshot/environment-setup-armv7a-vfp-neon-poky-linux-gnueabi 

# Setup your NFS exports
sudo vim /etc/exports
        /home/nick/Documents/BeagleBone/git-rootfs      *(rw,sync,no_subtree_check,no_root_squash)
# Extract the filesystem
cd /home/nick/Documents/BeagleBone/git-rootfs
sudo tar xvfj ../git-poky/core-image-minimal-beaglebone.tar.bz2 

# Extract the kernel
cd /work_directory_for_kernel/
tar xfz ../kernel_3.2.14.tar.gz

# Edit the source to remove the hardcoded path from firmware/am335x-pm-firmware.bin.gen.S

# Configuration and build kernel
# Copy the beagleboard defconfig to your .config
cp meta-ti/recipes-kernel/linux/linux-ti33x-psp-3.2/beaglebone/defconfig .config

# Make menuconfig and tweak what you want.
make ARCH=arm CROSS_COMPILE=arm-poky-linux-gnueabi- menuconfig

# Make the uImage
make ARCH=arm CROSS_COMPILE=arm-poky-linux-gnueabi- uImage
# Copy it to the TFTP
sudo cp arch/arm/boot/uImage /var/lib/tftpboot/uImage
