# @TODO: fix python-psyco

# stuff I like
sudo apt-get install ssh vim htop

# stuff needed by yocto
sudo apt-get install sed wget cvs subversion git-core coreutils \
     unzip texi2html texinfo libsdl1.2-dev docbook-utils gawk \
     python-pysqlite2 diffstat help2man make gcc build-essential \
     g++ desktop-file-utils chrpath libgl1-mesa-dev libglu1-mesa-dev \
     mercurial autoconf automake groff groff-base

# stuff to export stuff and to connect to board
sudo apt-get install conserver-server atftpd nfs-kernel-server

# to generate documentation
sudo apt-get install fop

# used for monitoring
sudo apt-get install iotop sysstat collectl bonnie++ smartmontools
