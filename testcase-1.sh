set -e  # Abort on failure
HERE=`pwd`

VERSION_TO_BUILD="master-2013-05-03_LOCAL-2013-04-25-c09866b-armv7a-beaglebone"
CONFIG_FILE="/opt/yocto/build/master-2013-05-03_LOCAL-2013-04-25-c09866b-armv7a-beaglebone/conf/local.conf"
#WHAT_TO_BUILD="core-image-sato-sdk"
WHAT_TO_BUILD="core-image-minimal"
#WHAT_TO_BUILD="meta-toolchain-sdk"
#WHAT_TO_BUILD="world"

#TIME="/usr/bin/time -f'%E'"
TIME="/usr/bin/time -f 'wallclock: %E user: %U kernel: %S'"
#TIME="dumbbench -- "

LOGTIME=$(date +%y%m%d-%H%M%S)
echo ${LOGTIME} 

if [ ! -d "${HERE}/../logfiles" ]; then
  mkdir ${HERE}/../logfiles
fi

LOG="${HERE}/../logfiles/20${LOGTIME}-log.txt"

cd /work/${USER}/poky
source oe-init-build-env /opt/yocto/build/${VERSION_TO_BUILD}

if swapon -s | grep partition; then
   echo "+ swap we have"
   command="sudo swapoff -a"
   echo "----->" >> ${LOG}
   echo "$(date)" >> ${LOG}
   echo "+ ${command}" >> ${LOG}
   eval ${command} | tee --append ${LOG}
   echo "<-----" >> ${LOG}
else
   echo "+ no swap there is"
   echo "+ no need to turn off"
fi

command="cat ${CONFIG_FILE}"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval ${command} | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="uptime"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval ${command} | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="cat /proc/meminfo"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval ${command} | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="ps aux"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval ${command} | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="${TIME} rm -rf /opt/yocto/build/${VERSION_TO_BUILD}/sstate-cache"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval "${command} 2>&1" | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="ps aux"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval ${command} | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="${TIME} rm -rf /opt/yocto/build/${VERSION_TO_BUILD}/tmp"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval "${command} 2>&1" | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="ps aux"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval ${command} | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

#command="${TIME} bitbake -c cleansstate ${WHAT_TO_BUILD}"
#echo "----->" >> ${LOG}
#echo "$(date)" >> ${LOG}
#echo "+ ${command}" >> ${LOG}
#mala=$((${command}) 2>&1)
#echo "${mala:${#mala} - 300}" >> ${LOG}
#echo "<-----" >> ${LOG}

#command="${TIME} bitbake -c fetchall  ${WHAT_TO_BUILD}"
#echo "----->" >> ${LOG}
#echo "$(date)" >> ${LOG}
#echo "+ ${command}" >> ${LOG}
#mala=$((${command}) 2>&1)
#echo "${mala:${#mala} - 300}" >> ${LOG}
#echo "<-----" >> ${LOG}

command="${TIME} bitbake -k  ${WHAT_TO_BUILD}"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
# ---> just the last 300 lines 
#mala=$((${command}) 2>&1)
#echo "${mala:${#mala} - 300}" >> ${LOG}
# <--- just the last 300 lines
#${TIME} bitbake -k ${WHAT_TO_BUILD} 2>&1 | tee --append ${LOG}
#eval ${command} | tee --append ${LOG}
eval "${command} 2>&1" | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

command="ps aux"
echo "----->" >> ${LOG}
echo "$(date)" >> ${LOG}
echo "+ ${command}" >> ${LOG}
eval ${command} | tee --append ${LOG}
echo "$(date)" >> ${LOG}
echo "<-----" >> ${LOG}

#command="${TIME} bitbake -k  ${WHAT_TO_BUILD}"
#echo "----->" >> ${LOG}
#echo "$(date)" >> ${LOG}
#echo "+ ${command}" >> ${LOG}
# ---> just the last 300 lines 
#mala=$((${command}) 2>&1)
#echo "${mala:${#mala} - 300}" >> ${LOG}
# <--- just the last 300 lines
#eval ${command} | tee --append ${LOG}
#eval "${command} 2>&1" | tee --append ${LOG}
#echo "$(date)" >> ${LOG}
#echo "<-----" >> ${LOG}

#command="ps aux"
#echo "----->" >> ${LOG}
#echo "$(date)" >> ${LOG}
#echo "+ ${command}" >> ${LOG}
#eval ${command} | tee --append ${LOG}
#echo "$(date)" >> ${LOG}
#echo "<-----" >> ${LOG}

cat ${LOG}
